package app;

import task.SimpleRunnableTask;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class RunnableExample {
    public static void main(String[] args) {
        Thread thread = new Thread(new SimpleRunnableTask("With Thread"));
        thread.start();
        new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("Run with anonymous class");
            }
        }).start();
        thread = new Thread(() -> System.out.println("Lambda in new Thread"));
        thread.start();

        ExecutorService executorService = Executors.newSingleThreadExecutor();
        executorService.submit(new SimpleRunnableTask("With ExecutorService"));
        executorService.submit(() -> System.out.println("Executed with lambda"));

        executorService.shutdown();
    }
}
