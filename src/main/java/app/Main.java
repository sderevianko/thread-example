package app;

import threads.FirstThread;
import threads.SecondThread;

import static java.lang.System.*;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        /*Thread t = Thread.currentThread(); // получаем главный поток
        out.println(t.getName()); // main
        out.println(t); // Thread[main,5,main]
        out.println(t.getThreadGroup().getName()); // main

        t.setName("THREAD"); // изменить имя потока
        out.println(t.getName()); // THREAD
        out.println(t); // Thread[THREAD,5,main]
        out.println(t.getThreadGroup().getName()); // main

        out.println(t.getPriority()); // вывести приоритет потока (5)

        t.setPriority(7); // установить приоритет потока от 1 до 10
        out.println(t.getPriority()); // 7
        out.println(t.getId()); // 1
        out.println(t.getState()); // RUNNABLE*/
        //State.

        ///////////////////////////////////////////////////////////////////////////////////////
        /*out.println("Main thread started...");
        var threads = new FirstThread("threads");
        threads.start();
        out.println("Main thread finished...");*/

        ///////////////////////////////////////////////////////////////////////////////////////
        /*out.println("Main thread started...");
        FirstThread thread = new FirstThread("FirstThread");
        thread.start();
        try{
            thread.join();
        }
        catch(InterruptedException e){

            out.printf("%s has been interrupted", thread.getName());
        }
        out.println("Main thread finished...");*/
        ///////////////////////////////////////////////////////////////////////////////////////

        /*System.out.println("Main thread started...");
        SecondThread t = new SecondThread("SecondThread");
        t.start();
        try{
            Thread.sleep(200);
            t.interrupt();
            Thread.sleep(150);
        }
        catch(InterruptedException e){
            System.out.println("SecondThread has been interrupted");
        }
        System.out.println("Main thread finished...");*/
    }
}
