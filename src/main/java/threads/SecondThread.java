package threads;

import static java.lang.System.*;

public class SecondThread extends Thread{

    public SecondThread(String name){
        super(name);
    }

    @Override
    public void run(){

        out.printf("%s started... \n", Thread.currentThread().getName());
        int counter = 1; // счетчик циклов
        while(!isInterrupted()) {
            out.println("Loop " + counter++);
        }
        out.printf("%s finished... \n", Thread.currentThread().getName());
    }
}
