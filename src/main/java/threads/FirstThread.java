package threads;

import static java.lang.System.*;

public class FirstThread extends Thread{

    public FirstThread(String name){
        super(name);
    }

    @Override
    public void run(){
        out.printf("%s started... \n", Thread.currentThread().getName());
        try {
            Thread.sleep(5000);
        }
        catch(InterruptedException e){
            out.println("Thread has been interrupted");
        }
        out.printf("%s finished... \n", Thread.currentThread().getName());
    }
}
