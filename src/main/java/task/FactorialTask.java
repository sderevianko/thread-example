package task;

import exception.InvalidParamaterException;

import java.util.concurrent.Callable;

public class FactorialTask implements Callable<Integer> {
    private final int number;

    public FactorialTask(int number) {
        this.number = number;
    }

    @Override
    public Integer call() throws InvalidParamaterException {
        System.out.println("Callable task executed");
        if (number < 0) {
            throw new InvalidParamaterException("Number must be positive");
        }
        return factorial(number);
    }

    private int factorial(int number) {
        if (number == 0 || number == 1) {
            return 1;
        }
        return number * factorial(number - 1);
    }
}
