package task;

public class SimpleRunnableTask implements Runnable {
    private final String message;

    public SimpleRunnableTask(String message) {
        this.message = message;
    }

    @Override
    public void run() {
        System.out.println("Runnable task executed. " + message);
    }
}
